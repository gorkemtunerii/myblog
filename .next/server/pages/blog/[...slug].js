/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(function() {
var exports = {};
exports.id = "pages/blog/[...slug]";
exports.ids = ["pages/blog/[...slug]"];
exports.modules = {

/***/ "./components/mdx-components.js":
/*!**************************************!*\
  !*** ./components/mdx-components.js ***!
  \**************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"mdxComponents\": function() { return /* binding */ mdxComponents; }\n/* harmony export */ });\nconst mdxComponents = {};//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9teWJsb2cvLi9jb21wb25lbnRzL21keC1jb21wb25lbnRzLmpzP2ZkMmUiXSwibmFtZXMiOlsibWR4Q29tcG9uZW50cyJdLCJtYXBwaW5ncyI6Ijs7OztBQUFPLE1BQU1BLGFBQWEsR0FBQyxFQUFwQiIsImZpbGUiOiIuL2NvbXBvbmVudHMvbWR4LWNvbXBvbmVudHMuanMuanMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY29uc3QgbWR4Q29tcG9uZW50cz17XG4gICAgXG59Il0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./components/mdx-components.js\n");

/***/ }),

/***/ "./pages/blog/[...slug].js":
/*!*********************************!*\
  !*** ./pages/blog/[...slug].js ***!
  \*********************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": function() { return /* binding */ PostPage; },\n/* harmony export */   \"getStaticPaths\": function() { return /* binding */ getStaticPaths; },\n/* harmony export */   \"getStaticProps\": function() { return /* binding */ getStaticProps; }\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var next_mdx_server__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next-mdx/server */ \"next-mdx/server\");\n/* harmony import */ var next_mdx_server__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_mdx_server__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var next_mdx_client__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next-mdx/client */ \"next-mdx/client\");\n/* harmony import */ var next_mdx_client__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_mdx_client__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var _components_mdx_components__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../components/mdx-components */ \"./components/mdx-components.js\");\n\nvar _jsxFileName = \"/Users/gorkemtuneri/Desktop/Workspace/myblog/pages/blog/[...slug].js\";\n\n\n\n\nfunction PostPage({\n  page\n}) {\n  const content = (0,next_mdx_client__WEBPACK_IMPORTED_MODULE_3__.useHydrate)(page, {\n    components: _components_mdx_components__WEBPACK_IMPORTED_MODULE_4__.mdxComponents\n  });\n  console.log(page);\n  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"div\", {\n    className: \"site-container\",\n    children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"article\", {\n      className: \"prose\",\n      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"h1\", {\n        className: \"text-2xl font-bold\",\n        children: page.frontMatter.title\n      }, void 0, false, {\n        fileName: _jsxFileName,\n        lineNumber: 15,\n        columnNumber: 11\n      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"p\", {\n        children: page.frontMatter.excerpt\n      }, void 0, false, {\n        fileName: _jsxFileName,\n        lineNumber: 16,\n        columnNumber: 11\n      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"hr\", {}, void 0, false, {\n        fileName: _jsxFileName,\n        lineNumber: 17,\n        columnNumber: 11\n      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"p\", {\n        children: content\n      }, void 0, false, {\n        fileName: _jsxFileName,\n        lineNumber: 18,\n        columnNumber: 11\n      }, this)]\n    }, void 0, true, {\n      fileName: _jsxFileName,\n      lineNumber: 14,\n      columnNumber: 7\n    }, this)\n  }, void 0, false, {\n    fileName: _jsxFileName,\n    lineNumber: 13,\n    columnNumber: 10\n  }, this);\n}\nasync function getStaticPaths() {\n  return {\n    paths: await (0,next_mdx_server__WEBPACK_IMPORTED_MODULE_2__.getMdxPaths)(\"post\"),\n    fallback: false\n  };\n}\nasync function getStaticProps(context) {\n  const page = await (0,next_mdx_server__WEBPACK_IMPORTED_MODULE_2__.getMdxNode)(\"post\", context);\n  return {\n    props: {\n      page\n    }\n  };\n}//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9teWJsb2cvLi9wYWdlcy9ibG9nL1suLi5zbHVnXS5qcz80YzVhIl0sIm5hbWVzIjpbIlBvc3RQYWdlIiwicGFnZSIsImNvbnRlbnQiLCJ1c2VIeWRyYXRlIiwiY29tcG9uZW50cyIsIm1keENvbXBvbmVudHMiLCJjb25zb2xlIiwibG9nIiwiZnJvbnRNYXR0ZXIiLCJ0aXRsZSIsImV4Y2VycHQiLCJnZXRTdGF0aWNQYXRocyIsInBhdGhzIiwiZ2V0TWR4UGF0aHMiLCJmYWxsYmFjayIsImdldFN0YXRpY1Byb3BzIiwiY29udGV4dCIsImdldE1keE5vZGUiLCJwcm9wcyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUdlLFNBQVNBLFFBQVQsQ0FBa0I7QUFBRUM7QUFBRixDQUFsQixFQUE0QjtBQUUzQyxRQUFNQyxPQUFPLEdBQUdDLDJEQUFVLENBQUNGLElBQUQsRUFBTTtBQUM1QkcsY0FBVSxFQUFHQyxxRUFBYUE7QUFERSxHQUFOLENBQTFCO0FBR0FDLFNBQU8sQ0FBQ0MsR0FBUixDQUFZTixJQUFaO0FBQ0Usc0JBQU87QUFBSyxhQUFTLEVBQUMsZ0JBQWY7QUFBQSwyQkFDSDtBQUFTLGVBQVMsRUFBQyxPQUFuQjtBQUFBLDhCQUNJO0FBQUksaUJBQVMsRUFBQyxvQkFBZDtBQUFBLGtCQUFvQ0EsSUFBSSxDQUFDTyxXQUFMLENBQWlCQztBQUFyRDtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBREosZUFFSTtBQUFBLGtCQUFJUixJQUFJLENBQUNPLFdBQUwsQ0FBaUJFO0FBQXJCO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FGSixlQUdJO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FISixlQUlJO0FBQUEsa0JBQUlSO0FBQUo7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQUpKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURHO0FBQUE7QUFBQTtBQUFBO0FBQUEsVUFBUDtBQVFEO0FBRU0sZUFBZVMsY0FBZixHQUFnQztBQUNyQyxTQUFPO0FBQ0xDLFNBQUssRUFBRSxNQUFNQyw0REFBVyxDQUFDLE1BQUQsQ0FEbkI7QUFFTEMsWUFBUSxFQUFFO0FBRkwsR0FBUDtBQUlEO0FBRU0sZUFBZUMsY0FBZixDQUE4QkMsT0FBOUIsRUFBdUM7QUFDNUMsUUFBTWYsSUFBSSxHQUFHLE1BQU1nQiwyREFBVSxDQUFDLE1BQUQsRUFBU0QsT0FBVCxDQUE3QjtBQUVBLFNBQU87QUFDTEUsU0FBSyxFQUFFO0FBQ0xqQjtBQURLO0FBREYsR0FBUDtBQUtEIiwiZmlsZSI6Ii4vcGFnZXMvYmxvZy9bLi4uc2x1Z10uanMuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QgZnJvbSBcInJlYWN0XCI7XG5pbXBvcnQgeyBnZXRNZHhOb2RlLCBnZXRNZHhQYXRocyB9IGZyb20gXCJuZXh0LW1keC9zZXJ2ZXJcIjtcbmltcG9ydCB7IHVzZUh5ZHJhdGUgfSBmcm9tIFwibmV4dC1tZHgvY2xpZW50XCJcbmltcG9ydCB7IG1keENvbXBvbmVudHMgfSBmcm9tIFwiLi4vLi4vY29tcG9uZW50cy9tZHgtY29tcG9uZW50c1wiO1xuXG5cbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIFBvc3RQYWdlKHsgcGFnZSB9KSB7XG5cbmNvbnN0IGNvbnRlbnQgPSB1c2VIeWRyYXRlKHBhZ2Use1xuICAgIGNvbXBvbmVudHMgOiBtZHhDb21wb25lbnRzXG59KVxuY29uc29sZS5sb2cocGFnZSlcbiAgcmV0dXJuIDxkaXYgY2xhc3NOYW1lPVwic2l0ZS1jb250YWluZXJcIj5cbiAgICAgIDxhcnRpY2xlIGNsYXNzTmFtZT1cInByb3NlXCIgPlxuICAgICAgICAgIDxoMSBjbGFzc05hbWU9XCJ0ZXh0LTJ4bCBmb250LWJvbGRcIj57cGFnZS5mcm9udE1hdHRlci50aXRsZX08L2gxPlxuICAgICAgICAgIDxwPntwYWdlLmZyb250TWF0dGVyLmV4Y2VycHR9PC9wPlxuICAgICAgICAgIDxoci8+XG4gICAgICAgICAgPHA+e2NvbnRlbnR9PC9wPlxuICAgICAgPC9hcnRpY2xlPlxuICA8L2Rpdj47XG59XG5cbmV4cG9ydCBhc3luYyBmdW5jdGlvbiBnZXRTdGF0aWNQYXRocygpIHtcbiAgcmV0dXJuIHtcbiAgICBwYXRoczogYXdhaXQgZ2V0TWR4UGF0aHMoXCJwb3N0XCIpLFxuICAgIGZhbGxiYWNrOiBmYWxzZSxcbiAgfTtcbn1cblxuZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIGdldFN0YXRpY1Byb3BzKGNvbnRleHQpIHtcbiAgY29uc3QgcGFnZSA9IGF3YWl0IGdldE1keE5vZGUoXCJwb3N0XCIsIGNvbnRleHQpO1xuXG4gIHJldHVybiB7XG4gICAgcHJvcHM6IHtcbiAgICAgIHBhZ2UsXG4gICAgfSxcbiAgfTtcbn1cbiJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./pages/blog/[...slug].js\n");

/***/ }),

/***/ "next-mdx/client":
/*!**********************************!*\
  !*** external "next-mdx/client" ***!
  \**********************************/
/***/ (function(module) {

"use strict";
module.exports = require("next-mdx/client");;

/***/ }),

/***/ "next-mdx/server":
/*!**********************************!*\
  !*** external "next-mdx/server" ***!
  \**********************************/
/***/ (function(module) {

"use strict";
module.exports = require("next-mdx/server");;

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/***/ (function(module) {

"use strict";
module.exports = require("react");;

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ (function(module) {

"use strict";
module.exports = require("react/jsx-dev-runtime");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = function(moduleId) { return __webpack_require__(__webpack_require__.s = moduleId); }
var __webpack_exports__ = (__webpack_exec__("./pages/blog/[...slug].js"));
module.exports = __webpack_exports__;

})();